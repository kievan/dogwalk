package homework.one.gameplay;

import homework.one.model.*;
import homework.one.service.ArtifactCounter;
import homework.one.service.SceneRenderer;
import homework.one.service.SpriteMover;
import homework.one.service.SpriteShuffler;

import java.util.Scanner;

public class DogWalker {
    private static final String MOVE_UP = "w";
    private static final String MOVE_DOWN = "s";
    private static final String MOVE_LEFT = "a";
    private static final String MOVE_RIGHT = "d";
    private static final String EXIT = "e";

    private BoardSquareScene boardSquareScene;
    private DogSprite dogSprite;
    private Sprite[] artifacts = {
        new BombSprite(), new BombSprite(), new BombBigSprite(), new BombBigSprite(),
        new MedkitSprite(), new MedkitSprite(), new MedkitBigSprite(), new MedkitBigSprite()
    };


    public DogWalker() {
        dogSprite = new DogSprite();
    }

    public void start(int boardSideLength) {
        boardSquareScene = new BoardSquareScene(boardSideLength);
        SpriteMover spriteMover = new SpriteMover(boardSquareScene);

        placeBlankSprites();
        shuffleSprites();
        renderScene();

        Scanner scanner = new Scanner(System.in);
        String input;
        String exit;
        do {
            input = scanner.next().toLowerCase();
            switch (input) {
                case MOVE_UP:
                    spriteMover.moveUp(dogSprite);
                    break;

                case MOVE_DOWN:
                    spriteMover.moveDown(dogSprite);
                    break;

                case MOVE_LEFT:
                    spriteMover.moveLeft(dogSprite);
                    break;

                case MOVE_RIGHT:
                    spriteMover.moveRight(dogSprite);
                    break;

                case EXIT:
                    break;

                default:
                    break;
            }
            renderScene();
            exit = checkDogHealth();
            exit = !exit.equals(EXIT) ? checkArtifactsOpened() : EXIT;
        } while (!input.equals(EXIT) && !exit.equals(EXIT) );
    }


    private void placeBlankSprites() {
        for (int x = 0; x < boardSquareScene.getWidth(); x++) {
            for (int y = 0; y < boardSquareScene.getHeight(); y++) {
                boardSquareScene.getSpriteCells()[x][y] = new BlankSprite();
            }
        }
    }

    private void shuffleSprites() {
        SpriteShuffler spriteShuffler = new SpriteShuffler();

        for (int i = 0; i < artifacts.length; i++) {
            spriteShuffler.shuffle(boardSquareScene, artifacts[i]);
        }

        spriteShuffler.shuffle(boardSquareScene, dogSprite);
    }

    private void renderScene() {
        SceneRenderer sr = new SceneRenderer();
        sr.render(boardSquareScene, dogSprite);
    }

    private String checkDogHealth() {
        String exit = dogSprite.getHealthPoints() <= 0 ? EXIT : "";
        if(!exit.isEmpty()) {
            printLine("");
            printLine("GAME OVER!");
        }
        return exit;
    }

    private String checkArtifactsOpened() {
        ArtifactCounter ac = new ArtifactCounter();
        String exit = ac.count(boardSquareScene) == 0 ? EXIT : "";
        if(!exit.isEmpty()) {
            printLine("YOU WON!");
        }
        return exit;
    }



    private void printLine(String line) {
        System.out.println(line);
    }
}
