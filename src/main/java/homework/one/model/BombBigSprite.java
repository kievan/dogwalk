package homework.one.model;

public class BombBigSprite extends Sprite {

    public BombBigSprite() {
        healthPoints = -40;
    }

    @Override
    public String getSpriteText() {
        return "**";
    }
}
