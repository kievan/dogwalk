package homework.one.model;

public class BlankSprite extends Sprite {

    public BlankSprite() {
        healthPoints = 0;
    }

    @Override
    public String getSpriteText() {
        return "_ ";
    }
}
