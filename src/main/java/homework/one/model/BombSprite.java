package homework.one.model;

public class BombSprite extends Sprite {

    public BombSprite() {
        healthPoints = -20;
    }

    @Override
    public String getSpriteText() {
        return "* ";
    }
}
