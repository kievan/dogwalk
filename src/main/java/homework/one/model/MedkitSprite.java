package homework.one.model;

public class MedkitSprite extends Sprite {

    public MedkitSprite() {
        healthPoints = 20;
    }

    @Override
    public String getSpriteText() {
        return "M ";
    }
}
