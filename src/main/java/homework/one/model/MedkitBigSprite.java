package homework.one.model;

public class MedkitBigSprite extends Sprite {

    public MedkitBigSprite() {
        healthPoints = 20;
    }

    @Override
    public String getSpriteText() {
        return "MM";
    }
}
