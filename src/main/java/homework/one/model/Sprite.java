package homework.one.model;

public abstract class Sprite {
    public static final int MAX_HEALTH_POINTS = 100;

    protected int healthPoints;

    private int positionX;
    private int positionY;

    public Sprite() {
        this.positionX = 0;
        this.positionY = 0;
    }

    public Sprite(int positionX, int positionY) {
        this.positionX = positionX;
        this.positionY = positionY;
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public int getHealthPoints() {
        return healthPoints;
    }

    public void setHealthPoints(int healthPoints) {
        this.healthPoints = healthPoints;
    }

    public abstract String getSpriteText();
}
