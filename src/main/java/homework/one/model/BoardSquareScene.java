package homework.one.model;

public class BoardSquareScene {
    private int sideLength;
    private int width;
    private int height;
    private Sprite[][] spriteCells;

    public BoardSquareScene(int sideLength) {
        this.sideLength = sideLength;
        width = sideLength;
        height = sideLength;
        spriteCells = new Sprite[width][height];
    }

    public int getSideLength() {
        return sideLength;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Sprite[][] getSpriteCells() {
        return spriteCells;
    }

    public void setSpriteCellAt(Sprite sprite) {
        this.spriteCells[sprite.getPositionX()][sprite.getPositionY()] = sprite;
    }
}
