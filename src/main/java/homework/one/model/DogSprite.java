package homework.one.model;

public class DogSprite extends Sprite {
    private int healthPoints;

    public DogSprite() {
        healthPoints = 100;
    }

    @Override
    public int getHealthPoints() {
        return healthPoints;
    }

    @Override
    public void setHealthPoints(int healthPoints) {
        this.healthPoints = healthPoints;
    }

    @Override
    public String getSpriteText() {
        return "@ ";
    }
}
