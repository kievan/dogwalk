package homework.one;

import homework.one.gameplay.DogWalker;
import homework.one.ui.Menu;

public class Application {
    public static void main(String[] args) {
        DogWalker dw = new DogWalker();
        Menu menu = new Menu(dw);
        menu.run();
    }
}
