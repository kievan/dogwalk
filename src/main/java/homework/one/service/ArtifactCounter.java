package homework.one.service;

import homework.one.model.BlankSprite;
import homework.one.model.BoardSquareScene;
import homework.one.model.DogSprite;
import homework.one.model.Sprite;

public class ArtifactCounter {
    public int count(BoardSquareScene scene) {
        int artifacts = 0;
        for (int y = 0; y < scene.getWidth(); y++) {
            for (int x = 0; x < scene.getHeight(); x++) {
                Sprite sprite = scene.getSpriteCells()[x][y];
                artifacts = !(sprite instanceof BlankSprite) && !(sprite instanceof DogSprite) ? artifacts + 1 : artifacts;
            }
        }

        return artifacts;
    }
}
