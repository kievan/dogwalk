package homework.one.service;

import homework.one.model.BlankSprite;
import homework.one.model.BoardSquareScene;
import homework.one.model.Sprite;

public class SpriteShuffler {
    public Sprite[][] shuffle(BoardSquareScene scene, Sprite sprite) {
        Sprite[][] spriteCells = scene.getSpriteCells();

        boolean isBlankSprite;
        do {
            sprite.setPositionX((int) (Math.random() * scene.getWidth()));
            sprite.setPositionY((int) (Math.random() * scene.getHeight()));
            Sprite blankSprite = spriteCells[sprite.getPositionX()][sprite.getPositionY()];

            isBlankSprite = blankSprite instanceof BlankSprite;
        } while (!isBlankSprite);

        spriteCells[sprite.getPositionX()][sprite.getPositionY()] = sprite;

        return spriteCells;
    }
}
