package homework.one.service;

import homework.one.model.BlankSprite;
import homework.one.model.BoardSquareScene;
import homework.one.model.Sprite;

public class SpriteMover {
    private BoardSquareScene scene;

    public SpriteMover(BoardSquareScene scene) {
        this.scene = scene;
    }

    public void moveUp(Sprite sprite) {
        Sprite cellWithBlankSprite = fetchBlankSpriteAtPositionOf(sprite);
        sprite.setPositionY(fetchSceneBoundedCoordinate(sprite.getPositionY() - 1));

        updateSpriteHealth(sprite);

        this.placeSceneSpriteAtPositionOf(sprite);
        this.placeBlankSpriteAtPositionOf(cellWithBlankSprite);
    }

    public void moveDown(Sprite sprite) {
        Sprite cellWithBlankSprite = fetchBlankSpriteAtPositionOf(sprite);
        sprite.setPositionY(fetchSceneBoundedCoordinate(sprite.getPositionY() + 1));

        updateSpriteHealth(sprite);

        this.placeSceneSpriteAtPositionOf(sprite);
        this.placeBlankSpriteAtPositionOf(cellWithBlankSprite);
    }

    public void moveLeft(Sprite sprite) {
        Sprite cellWithBlankSprite = fetchBlankSpriteAtPositionOf(sprite);
        sprite.setPositionX(fetchSceneBoundedCoordinate(sprite.getPositionX() - 1));

        updateSpriteHealth(sprite);

        this.placeSceneSpriteAtPositionOf(sprite);
        this.placeBlankSpriteAtPositionOf(cellWithBlankSprite);
    }

    public void moveRight(Sprite sprite) {
        Sprite cellWithBlankSprite = fetchBlankSpriteAtPositionOf(sprite);
        sprite.setPositionX(fetchSceneBoundedCoordinate(sprite.getPositionX() + 1));

        updateSpriteHealth(sprite);

        this.placeSceneSpriteAtPositionOf(sprite);
        this.placeBlankSpriteAtPositionOf(cellWithBlankSprite);
    }

    private void placeSceneSpriteAtPositionOf(Sprite sprite) {
        scene.setSpriteCellAt(sprite);
    }

    private void placeBlankSpriteAtPositionOf(Sprite fillCell) {
        Sprite blank = new BlankSprite();
        blank.setPositionX(fillCell.getPositionX());
        blank.setPositionY(fillCell.getPositionY());
        this.placeSceneSpriteAtPositionOf(blank);
    }

    private Sprite fetchBlankSpriteAtPositionOf(Sprite sprite) {
        Sprite blank = new BlankSprite();
        blank.setPositionX(sprite.getPositionX());
        blank.setPositionY(sprite.getPositionY());
        return blank;
    }

    private void updateSpriteHealth(Sprite sprite) {
        Sprite toCell = fetchSpriteAtPositionOf(sprite);
        sprite.setHealthPoints(fetchDogSpriteBoundedHealthPoints(sprite.getHealthPoints() + toCell.getHealthPoints()));
    }

    private int fetchDogSpriteBoundedHealthPoints(int healthPoints) {
        int boundedHP = Math.max(healthPoints, 0);
        boundedHP = Math.min(boundedHP, Sprite.MAX_HEALTH_POINTS);

        return boundedHP;
    }

    private Sprite fetchSpriteAtPositionOf(Sprite sprite) {
        return scene.getSpriteCells()[sprite.getPositionX()][sprite.getPositionY()];
    }

    private int fetchSceneBoundedCoordinate(int coordinate) {
        int boundedCoordinate = coordinate < 0 ? (scene.getSideLength() - 1) : coordinate;
        boundedCoordinate = boundedCoordinate > (scene.getSideLength() - 1) ? 0 : boundedCoordinate;

        return boundedCoordinate;
    }
}

