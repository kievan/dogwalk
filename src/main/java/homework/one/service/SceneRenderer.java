package homework.one.service;

import homework.one.model.BoardSquareScene;
import homework.one.model.Sprite;

public class SceneRenderer {
    public void render(BoardSquareScene scene, Sprite dogSprite) {
        int dogHealthPoints =  dogSprite.getHealthPoints();
        printLine(String.format("Dog's health: %d", dogHealthPoints));
        printLine("");
        for (int y = 0; y < scene.getWidth(); y++) {
            for (int x = 0; x < scene.getHeight(); x++) {
                Sprite sprite = scene.getSpriteCells()[x][y];
                String spriteText = sprite.getSpriteText();
                print(spriteText);
            }
            printLine("");
        }
    }

    private static void print(String symbol) {
        System.out.print(symbol);
    }

    private static void printLine(String line) {
        System.out.println(line);
    }
}
