package homework.one.ui;

import homework.one.gameplay.DogWalker;

import java.util.Scanner;

public class Menu {
    public static final String GREETING = "Welcome to Dog Walker Game!";
    public static final String INSTRUCTIONS = "Enter square board side length and play the game," +
        "\nbeware of the bombs,\nand be on the lookout for medkits\n Good luck!";
    public static final String FAREWELL = "Farewell!";
    public static final String REPEAT_PROMPT = "Would you like to go again (y/n)?";
    public static final String ANSWER_YES = "y";
    public static final String BOARD_SIDE_LENGTH_PROMPT = "Enter square board side length (min. 5, max. 10) >>> ";

    private Scanner sc;
    private DogWalker dogWalker;

    public Menu(DogWalker dogWalker) {
        this.dogWalker = dogWalker;
    }

    public void run() {
        printLine(GREETING);
        printLine(INSTRUCTIONS);
        sc = new Scanner(System.in);
        boolean repeat;

        do {
            printLine(BOARD_SIDE_LENGTH_PROMPT);
            int boardSideLength = readInt();
            dogWalker.start(boardSideLength);

            printLine(REPEAT_PROMPT);
            readLine();
            String input = readLine();
            repeat = input.equals(ANSWER_YES);
        } while (repeat);

        printLine(FAREWELL);
    }


    String readLine() {
        return sc.nextLine();
    }

    int readInt() {
        return sc.nextInt();
    }

    void printLine(String text) {
        System.out.println(text);
    }
}
